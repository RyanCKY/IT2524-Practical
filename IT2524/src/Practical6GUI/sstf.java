package Practical6GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import Practical6CLI.DiskParameter;

public class sstf extends JFrame {

	static Properties p = new Properties();
	static DiskParameter dp = null;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					sstf frame = new sstf();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public sstf() {
		setTitle("SSTF");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton button = new JButton("< Back");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				main main = new main();
				setVisible(false);
				main.setVisible(true);
				dispose();
			}
		});
		button.setBounds(10, 11, 89, 23);
		contentPane.add(button);
		
		JTextArea textArea = new JTextArea();
		textArea.setEditable(false);
		textArea.setBounds(10, 45, 414, 205);
		contentPane.add(textArea);
		
		JButton btnStart = new JButton("Start");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					p.load(new BufferedReader (new FileReader("diskq1.properties")));
					dp = new DiskParameter(p);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
					//DiskOptimisation.generateAnalysisFCFS();
				int location[]  = arrangeBySSTF (dp.getCurrent(), dp.getSequence());
				String sequence = "";
				String working1 = "";
				String working2 = "";
				int total = 0;
				sequence += dp.getCurrent();
				int previous = dp.getCurrent();
				for (int i = 0; i < location.length; i++) {
					int current = location[i];
					sequence += "," + current;
					int d = Math.abs(previous-current);
					
					working1 += "|" + previous + "-" + current + "|+";
					working2 += d + " + ";
					total += d;
					previous = current;
				}
				textArea.setText("SSTF" + '\n' + "====" + '\n' + "Order of Access: " + sequence + '\n' + "Total Distance = " + working1.substring(0, working1.length()-1) + '\n' + "               = " + working2.substring(0, working2.length()-2) + '\n' + "               = " + total + '\n');

			}
		});
		btnStart.setBounds(335, 11, 89, 23);
		contentPane.add(btnStart);
		
		JLabel lblFirstcomefirstserved = new JLabel("Shortest Seek Time First");
		lblFirstcomefirstserved.setHorizontalAlignment(SwingConstants.CENTER);
		lblFirstcomefirstserved.setBounds(109, 11, 211, 23);
		contentPane.add(lblFirstcomefirstserved);
		
		
	}
	
	private int[] arrangeBySSTF(int current, int sequence[]) {
		int n = sequence.length;
		int sstf[] = new int[n];
		for (int i = 0; i<n; i++) {
			sstf[i] = sequence[i];
		}
		
		int ii = -1;
		for (int i = 0; i<n; i++) {
			int minimum = Integer.MAX_VALUE;
			ii=i;
			for (int j = i; j<n; j++) {
				int distance = Math.abs(current - sstf[j]);
				if (distance < minimum) {
					ii = j;
					minimum = distance;
				}
			}
			int tmp  = sstf[i];
			sstf[i] = sstf[ii];
			sstf[ii] = tmp;
			current = sstf [i];
		}
		return sstf;
	}

}
