package Practical6GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Arrays;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import Practical6CLI.DiskParameter;

public class scan extends JFrame {

	static Properties p = new Properties();
	static DiskParameter dp = null;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					scan frame = new scan();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public scan() {
		setTitle("SCAN");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton button = new JButton("< Back");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				main main = new main();
				setVisible(false);
				main.setVisible(true);
				dispose();
			}
		});
		button.setBounds(10, 11, 89, 23);
		contentPane.add(button);
		
		JTextArea textArea = new JTextArea();
		textArea.setEditable(false);
		textArea.setBounds(10, 45, 414, 205);
		contentPane.add(textArea);
		
		JButton btnStart = new JButton("Start");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					p.load(new BufferedReader (new FileReader("diskq1.properties")));
					dp = new DiskParameter(p);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
					//DiskOptimisation.generateAnalysisFCFS();
				int location[] = arrangeBySCAN(dp.getCurrent(), dp.getSequence(), dp.getPrevious());
				String sequence = "";
				String working1 = "";
				String working2 = "";
				int total = 0;
				sequence += dp.getCurrent();
				int previous = dp.getCurrent();
				for (int i = 0; i < location.length; i++) {
					int current = location[i];
					sequence += "," + current;
					int d = Math.abs(previous-current);
					
					working1 += "|" + previous + "-" + current + "|+";
					working2 += d + " + ";
					total += d;
					previous = current;
				}
				textArea.setText("SCAN" + '\n' + "====" + '\n' + "Order of Access: " + sequence + '\n' + "Total Distance = " + working1.substring(0, working1.length()-1) + '\n' + "               = " + working2.substring(0, working2.length()-2) + '\n' + "               = " + total + '\n');

			}
		});
		btnStart.setBounds(335, 11, 89, 23);
		contentPane.add(btnStart);
		
		JLabel lblFirstcomefirstserved = new JLabel("SCAN");
		lblFirstcomefirstserved.setHorizontalAlignment(SwingConstants.CENTER);
		lblFirstcomefirstserved.setBounds(109, 11, 211, 23);
		contentPane.add(lblFirstcomefirstserved);
		
		
	}
	
	private int[] arrangeBySCAN(int current, int sequence[], int previous) {
	    // Assuming inward movement i.e going toward zero
	    int n = sequence.length;
	    int scan[] = new int[n + 1];
	    int temp[] = new int[n + 1];
	    
	    if (previous < current) {
		    for (int i = 0; i < n; i++) {
		        temp[i] = sequence[i];
		    }
		    temp[n] = 0;
		    Arrays.sort(temp);
	    }
	    else if (previous > current) {
	    	for (int i = 0; i < n; i++) {
		        temp[i] = sequence[i];
		    }
		    temp[n] = 4999;
		    Arrays.sort(temp);
	    }
	    
	    // find first element in array temp which is less than current
	    int index = 0;
	    for (int i = 1; i < temp.length; i++) {
	        if (temp[i] > current) {
	            index = i - 1;
	            break;
	        }
	    }
	    
	    int k = 0;
	    // Serve all request upto zero before move backward
	    for (int i = index; i >= 0; --i, ++k) {
	        scan[k] = temp[i];
	    }
	    // move forward from zero
	    for (int i = index + 1; i < temp.length; i++, ++k) {
	        scan[k] = temp[i];
	    }
	    return scan;
	}

}
