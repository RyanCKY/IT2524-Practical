package Practical6GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class properties extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					properties frame = new properties();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public properties() {
		setTitle("Visualisation Properties");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setResizable(false);
		
		JButton button = new JButton("< Back");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				main main = new main();
				setVisible(false);
				main.setVisible(true);
				dispose();
			}
		});
		button.setBounds(10, 11, 89, 23);
		contentPane.add(button);
		
		JTextArea textArea = new JTextArea();
		textArea.setEditable(false);
		textArea.setBounds(10, 66, 414, 184);
		textArea.setText("Cylinders=5000\nCurrent Position=143\nPrevious Position=152\nSequence=86,1470,913,1774,948,1509,1022,1750,130");
		contentPane.add(textArea);
		
		JLabel lblVisualisationProperties = new JLabel("Visualisation Properties");
		lblVisualisationProperties.setHorizontalAlignment(SwingConstants.CENTER);
		lblVisualisationProperties.setBounds(129, 15, 283, 23);
		contentPane.add(lblVisualisationProperties);
	}
}
