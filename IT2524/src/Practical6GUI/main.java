package Practical6GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class main extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					main frame = new main();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public main() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setResizable(false);
		
		JLabel lblDiskOptimisationVisualisation = new JLabel("Disk Optimisation Visualisation");
		lblDiskOptimisationVisualisation.setFont(new Font("Comic Sans MS", Font.PLAIN, 18));
		lblDiskOptimisationVisualisation.setHorizontalAlignment(SwingConstants.CENTER);
		lblDiskOptimisationVisualisation.setBounds(10, 11, 414, 48);
		contentPane.add(lblDiskOptimisationVisualisation);
		
		JButton btnFcfs = new JButton("FCFS");
		btnFcfs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fcfs fcfs = new fcfs();
				setVisible(false);
				fcfs.setVisible(true);
				dispose();
			}
		});
		btnFcfs.setBounds(20, 70, 187, 63);
		contentPane.add(btnFcfs);
		
		JButton btnSstf = new JButton("SSTF");
		btnSstf.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				sstf sstf = new sstf();
				setVisible(false);
				sstf.setVisible(true);
				dispose();
			}
		});
		btnSstf.setBounds(237, 70, 187, 63);
		contentPane.add(btnSstf);
		
		JButton btnScan = new JButton("SCAN");
		btnScan.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				scan scan = new scan();
				setVisible(false);
				scan.setVisible(true);
				dispose();
			}
		});
		btnScan.setBounds(20, 144, 187, 63);
		contentPane.add(btnScan);
		
		JButton btnLook = new JButton("LOOK");
		btnLook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				look look = new look();
				setVisible(false);
				look.setVisible(true);
				dispose();
			}
		});
		btnLook.setBounds(237, 144, 187, 63);
		contentPane.add(btnLook);
		
		JButton btnViewProperties = new JButton("View Properties");
		btnViewProperties.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				properties properties = new properties();
				setVisible(false);
				properties.setVisible(true);
				dispose();
			}
		});
		btnViewProperties.setBounds(20, 227, 187, 23);
		contentPane.add(btnViewProperties);
		
		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnExit.setBounds(237, 227, 187, 23);
		contentPane.add(btnExit);
	}
}
