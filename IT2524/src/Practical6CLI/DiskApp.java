package Practical6CLI;

import java.util.Scanner;

public class DiskApp {
	public static void main (String[] args) {
		Scanner in = new Scanner(System.in);
		
		System.out.println("DISK OPTIMISATION VISUALISATION");
		System.out.println("===============================");
		System.out.println("1. FCFS");
		System.out.println("2. SSTF");
		System.out.println("3. SCAN");
		System.out.println("4. LOOK");
		System.out.println("0. Exit");
		
		System.out.println("");
		
		System.out.print("Enter choice: ");
		int choice = in.nextInt();
		
		System.out.println("");
		
		if (choice == 0) {
			System.out.println("Goodbye!");
			System.exit(0);
		}
		else if (choice == 1) {
				new DiskOptimisation("diskq1.properties", choice);
		}
		else if (choice == 2) {
			new DiskOptimisation("diskq1.properties", choice);
		}
		else if (choice == 3) {
			new DiskOptimisation("diskq1.properties", choice);
		}
		else if (choice == 4) {
			new DiskOptimisation("diskq1.properties", choice);
		}
		else {
			System.out.print("ERROR RESET SYSTEM!");
			System.exit(0);
		}
		
		do {
			System.out.println("DISK OPTIMISATION VISUALISATION");
			System.out.println("===============================");
			System.out.println("1. FCFS");
			System.out.println("2. SSTF");
			System.out.println("3. SCAN");
			System.out.println("4. LOOK");
			System.out.println("0. Exit");
			
			System.out.println("");
			
			System.out.print("Enter choice: ");
			int choiceloop = in.nextInt();
			
			System.out.println("");
			
			if (choiceloop == 1) {
				new DiskOptimisation("diskq1.properties", choice);
		}
		else if (choiceloop == 2) {
			new DiskOptimisation("diskq1.properties", choice);
		}
		else if (choiceloop == 3) {
			new DiskOptimisation("diskq1.properties", choice);
		}
		else if (choiceloop == 4) {
			new DiskOptimisation("diskq1.properties", choice);
		}
		else if (choiceloop == 0) {
				System.out.println("Goodbye!");
				System.exit(0);
			}
		else {
			System.out.print("ERROR RESET SYSTEM!");
			System.exit(0);
		}
		} while (choice != 0);
	}
}
