package Practical6CLI;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Arrays;
import java.util.Properties;

public class DiskOptimisation {
	static Properties p = new Properties();
	static DiskParameter dp = null;
	
	
	//6
	public DiskOptimisation (String filename, int choice){
		try {
			p.load(new BufferedReader (new FileReader(filename)));
			dp = new DiskParameter(p);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if (choice == 1) {
			generateAnalysisFCFS();
		}
	}
	//5
	public static void generateAnalysisFCFS() {
		generateFCFS();
	}
	public void generateAnalysisSSTF() {
		generateSSTF();
	}
	public void generateAnalysisScan() {
		generateScan();
	}
	public void generateAnalysisLOOK() {
		generateLOOK();
	}
	public void generateAnalysis() {
		generateFCFS();
		generateSSTF();
		generateScan();
		generateLOOK();
	}
	//4
	public static void printSequence(String name, int location[]) {
		String sequence = "";
		String working1 = "";
		String working2 = "";
		int total = 0;
		sequence += dp.getCurrent();
		int previous = dp.getCurrent();
		for (int i = 0; i < location.length; i++) {
			int current = location[i];
			sequence += "," + current;
			int d = Math.abs(previous-current);
			
			working1 += "|" + previous + "-" + current + "|+";
			working2 += d + " + ";
			total += d;
			previous = current;
		}
		System.out.println(name + '\n'+"====");
		System.out.println("Order of Access: " + sequence);
		
		System.out.println("Total Distance = " + working1.substring(0, working1.length()-1));
		System.out.println("               = " + working2.substring(0, working2.length()-2));
		System.out.println("               = " + total + '\n');
		
	}
		//3
		public static void generateFCFS() {
			int location[] = dp.getSequence();
			printSequence("FCFS", location);
		}
		//2
		public void generateSSTF() {
			int location[]  = arrangeBySSTF (dp.getCurrent(), dp.getSequence());
			printSequence("SSTF", location);
		}
		public void generateScan() {
			int location[] = arrangeBySCAN(dp.getCurrent(), dp.getSequence());
			printSequence("SCAN", location);
		}
		public void generateLOOK() {
			int location[] = arrangeByLOOK(dp.getCurrent(), dp.getSequence());
			printSequence("LOOK", location);
		}
		//1
		private int[] arrangeBySSTF(int current, int sequence[]) {
			int n = sequence.length;
			int sstf[] = new int[n];
			for (int i = 0; i<n; i++) {
				sstf[i] = sequence[i];
			}
			
			int ii = -1;
			for (int i = 0; i<n; i++) {
				int minimum = Integer.MAX_VALUE;
				ii=i;
				for (int j = i; j<n; j++) {
					int distance = Math.abs(current - sstf[j]);
					if (distance < minimum) {
						ii = j;
						minimum = distance;
					}
				}
				int tmp  = sstf[i];
				sstf[i] = sstf[ii];
				sstf[ii] = tmp;
				current = sstf [i];
			}
			return sstf;
		}
		
		private int[] arrangeBySCAN(int current, int sequence[]) {
		    // Assuming inward movement i.e going toward zero
		    int n = sequence.length;
		    int scan[] = new int[n + 1];
		    int temp[] = new int[n + 1];
		    for (int i = 0; i < n; i++) {
		        temp[i] = sequence[i];
		    }
		    temp[n] = 0;
		    Arrays.sort(temp);
		    
		    // find first element in array temp which is less than current
		    int index = 0;
		    for (int i = 1; i < temp.length; i++) {
		        if (temp[i] > current) {
		            index = i - 1;
		            break;
		        }
		    }
		    
		    int k = 0;
		    // Serve all request upto zero before move backward
		    for (int i = index; i >= 0; --i, ++k) {
		        scan[k] = temp[i];
		    }
		    // move forward from zero
		    for (int i = index + 1; i < temp.length; i++, ++k) {
		        scan[k] = temp[i];
		    }
		    return scan;
		}
		
		private int[] arrangeByLOOK(int current, int sequence[]) {
		    // Assuming inward movement i.e going toward zero
		    int n = sequence.length;
		    int look[] = new int[n + 1];
		    int temp[] = new int[n + 1];
		    for (int i = 0; i < n; i++) {
		        temp[i] = sequence[i];
		    }
		    temp[n] = 0;
		    Arrays.sort(temp);
		    
		    // find first element in array temp which is less than current
		    int index = 0;
		    for (int i = 1; i < temp.length; i++) {
		        if (temp[i] > current) {
		            index = i - 1;
		            break;
		        }
		    }
		    
		    int k = 0;
		    // Serve all request upto zero before move backward
		    for (int i = index; i > 0; --i, ++k) {
		        look[k] = temp[i];
		    }
		    // move forward from zero
		    for (int i = index; i < temp.length; i++, ++k) {
		        look[k] = temp[i];
		    }
		    return look;
		}
}
